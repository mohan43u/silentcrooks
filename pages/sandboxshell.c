#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[], char *envp[])
{
  int iter = 0;

  if(setuid(65534) == -1)
    {
      perror("sandboxshell");
      return 1;
    }
  for(iter = 0; envp[iter] != NULL; iter++)
    {
      if(strstr(envp[iter], "SHELL=") == envp[iter])
	{
	  if(execve(strchr(envp[iter], '=') + 1, argv, envp) == -1)
	    {
	      perror("sandboxshell");
	      return 1;
	    }
	  break;
	}
    }
  return 0;
}
