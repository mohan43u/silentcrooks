#!/usr/bin/env node

/* modules */
var Child = require('child_process');
var Fs = require('fs');
var Util = require('util');
var Http = require('http');
var Url = require('url');
var Q = require('querystring');

var logHandle = function(obj) {
    Util.log(JSON.stringify(obj));
};

var run = function(cmdline, request, response) {
    var args = ['-c', cmdline];
    innerHTMLstdout = new String();
    innerHTMLstderr = new String();
    var child = Child.spawn('./sandboxshell', args);
    child.stdout.on('data', function(data) {
	var databuf = new Buffer(data);
	innerHTMLstdout = innerHTMLstdout + databuf.toString();
    });
    child.stderr.on('data', function(data) {
	var databuf = new Buffer(data);
	innerHTMLstderr = innerHTMLstderr + databuf.toString();
    });
    child.on('close', function(response, code, signal) {
	innerHTMLstdout = "<div id=\"stdout\"><pre>" + innerHTMLstdout + "</pre></div>";
	innerHTMLstderr = "<div id=\"stderr\"><pre>" + innerHTMLstderr + "</pre></div>";
	var innerHTMLexit = "<div id=\"exitstatus\"><pre>" + code + " " + signal + "</pre></div>";
	var innerHTML = "<div id=\"output\">" + innerHTMLstdout + innerHTMLstderr + innerHTMLexit + "</div>";
	response.write(innerHTML);
	response.end();
	delete this.innerHTMLstdout;
	delete this.innerHTMLstderr;
    }.bind(this, response));
    request.on('readable', function(child) {
	var chunk = new String();
	while(null != (chunk = this.read())) {
	    child.stdin.write(chunk.toString());
	}
	child.stdin.end();
    }.bind(request, child));
}

var serveStaticFiles = function(url, request, response) {
    var filename = url.pathname.slice(1);
    if(filename.length == 0) {
	if(url.query)
	{
	    logHandle({"LogType" : "Error",
		       "Error" : "Cannot serve dynamic requests",
		       "Url": url})
	    response.end();
	    return;
	}
	filename = "index.html";
    }
    Fs.readFile(filename, function(url, request, response, error, data) {
	if(error) {
	    logHandle({"LogType" : "Error",
		       "Error" : error});
	    response.end();
	    return;
	}
	response.write(data);
	response.end();
    }.bind(undefined, url, request, response));
};

var serveCommandline = function(query, request, response) {
    if(query && ('commandline' in query)) {
	run(query['commandline'], request, response);
    }
};

var serveBlog = function(query, request, response) {
    if(!query) {
	Fs.readFile('index.html.d/login.html', function(response, error, data) {
	    if(error) {
		logHandle({"LogType" : "Error",
			   "Error" : error});
	    }
	    this.loginHTML = data;
	    response.write(loginHTML);
	    response.end();
	}.bind(this, response));
    }
};

Http.createServer(function(request, response) {
    try {
	var url = Url.parse(request.url);
	var query = (url.query ? Q.parse(url.query) : undefined);
	if(url.path && url.path.search(/\/shell/) == 0) {
	    serveCommandline(query, request, response);
	}
	else if(url.path && url.path.search(/\/blog/) == 0) {
	    console.log("inside");
	    serveBlog(query, request, response);
	}
	else {
	    serveStaticFiles(url, request, response);
	}
	logHandle({"LogType" : "Info",
		   "Request" : [request.method,
				request.url,
				request.httpVersion,
				request.headers],
		   "Response" : response.statusCode});
    }
    catch(e) {
	response.end();
	logHandle({"LogType" : "Exception",
		   "Exception" : e,
		   "Request" : [request.method,
				request.url,
				request.httpVersion,
				request.headers],
		   "Response" : response.statusCode});
    }
}).listen(1337);
