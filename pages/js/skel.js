//local javascripts

var setMargin = function() {
    var footerHeight = document.getElementById("footer").clientHeight;
    document.getElementById("wrap").style.marginBottom = "-" + footerHeight + "px";
}

var removeNodeLogo = function() {
    if(String(window.location).search(/silentcrooks\.org/) == -1) {
	var nodeLogoElement = document.getElementById("nodelogo");
	if(nodeLogoElement && nodeLogoElement.parentNode) {
	    nodeLogoElement.parentNode.removeChild(nodeLogoElement);
	}
    }
}

var queryserver = function(){
    var runcommandline = document.getElementById("runbutton");
    runcommandline.addEventListener('click', function() {
	var httpRequest = XMLHttpRequest();
	httpRequest.addEventListener('readystatechange', function() {
	    if(httpRequest.readyState == httpRequest.DONE) {
		var httpResponse = httpRequest.response;
		var queryserver = document.getElementById("output");
		queryserver.innerHTML = httpResponse;
	    }
	});
	var commandline = document.getElementById("commandline").value;
	var stdin = document.getElementById("stdin").value;
	httpRequest.open("POST", url="/shell/?commandline=" + commandline);
	httpRequest.send(stdin);
    });
}

document.onreadystatechange = function() {
    if(document.readyState == "complete") {
	setMargin();
	removeNodeLogo();
	queryserver();
    }
}
